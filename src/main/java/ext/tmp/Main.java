package ext.tmp;

import java.io.File;
import java.util.Collection;

import ext.loader.JarLoader;

public class Main {
	public static void main(String[] args) throws Exception {
		File jar = new File("./src/main/java/ext/tmp/saludadorEjemplo.jar");
		Collection<Class<? extends Saludador>> classes = JarLoader.findClassesInFile(jar, Saludador.class);
		
		for(Saludador s : JarLoader.createInstances(classes))
			s.saludar();
	}
}
