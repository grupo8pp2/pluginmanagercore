package ext.loader;

public class JarLoadingException extends Exception {
	private static final long serialVersionUID = -5386473094391611606L;
	
	public JarLoadingException(Throwable cause) {
		super(cause.getMessage(), cause);
	}
}
