package ext.loader;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarLoader
{
	private JarLoader() {}
	
	public static <T> Set<T> createInstances(Collection<Class<? extends T>> classes, Object... constructorParameters) throws JarLoadingException {
		try {
			Set<T> ret = new HashSet<>();
			Class<?>[] parameterClasses = new Class<?>[constructorParameters.length];
			for(int i = 0; i < constructorParameters.length; i++)
				parameterClasses[i] = constructorParameters[i].getClass();
			
			for(Class<? extends T> c : classes)
				ret.add(c.getDeclaredConstructor(parameterClasses).newInstance(constructorParameters));
			
			return ret;
		} catch(InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			throw new JarLoadingException(e);
		}
	}
	
	public static <T> Set<Class<? extends T>> findClassesInFile(File jarFile, Class<T> c) throws JarLoadingException 
	{
		try {
			URL fileURL = jarFile.toURI().toURL();
			String jarURL = "jar:" + fileURL + "!/";
			URL[] urls = { new URL(jarURL) };
			try(URLClassLoader ucl = new URLClassLoader(urls)) {
				return getFromList(ucl, findClassNames(jarFile), c);
			}
		} catch(ClassNotFoundException | IOException e) {
			throw new JarLoadingException(e);
		}
	}
	
	@SuppressWarnings("unchecked") //I swear I'm checking!
	private static <T> Set<Class<? extends T>> getFromList(URLClassLoader ucl, List<String> classNames, Class<T> c) throws ClassNotFoundException
	{
		Set<Class<? extends T>> ret = new HashSet<>();
		for(String className : classNames)
		{
			Class<?> foundClass = Class.forName(className, true, ucl);
			if(c.isAssignableFrom(foundClass))
				ret.add((Class<? extends T>) foundClass);
		}
		return ret;
	}
	
	private static List<String> findClassNames(File pathToJar) throws IOException
	{
		List<String> ret = new ArrayList<>();
		
		JarFile jarFile = new JarFile(pathToJar);
		Enumeration<JarEntry> e = jarFile.entries();
		while (e.hasMoreElements()) {
		    JarEntry je = e.nextElement();
		    if(je.getName().endsWith(".class"))
			    ret.add(je.getName().replace(".class", "").replace("/", "."));
		}
		jarFile.close();
		
		return ret;
	}
}
